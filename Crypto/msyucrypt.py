from random import randint
import string

def test_for_protected(phrase):
    i = 60
    for i in range(60, 90):
        if phrase in protected_phrases[i]:
            return "W" + str(i)
    return None

def _assign_text_direction():
    pass

def _generate_null():
    return "N" + str(randint(60, 90))

def msyucrypt_transform(ciphertext, key):
    msyu_ciphertext = []    
    for letter in ciphertext:
        if 65 <= ord(letter) <= 90:
            msyu_ciphertext.append(letter + str(0) + str(randint(1, 5)))
        elif 97 <= ord(letter) <= 122:
            msyu_ciphertext.append(letter.upper() + str(randint(10, 15)))
    for letter in key:
        if 65 <= ord(letter) <= 90:
            msyu_ciphertext.append(letter + str(0) + str(randint(1, 5)))
        elif 97 <= ord(letter) <= 122:
            msyu_ciphertext.append(letter.upper() + str(randint(10, 15)))
    return ''.join(msyu_ciphertext)

protected_phrases = {
    60: "yes",
    61: "no",
    62: ["me", "myself", "i"],
    63: ["he", "him"],
    64: ["she", "her"],
    65: "now",
    66: ["this", "that"],
    67: ["you", "you're", "your"],
    68: "it",
    69: ["feel", "feeling"],
    70: "interest",
    71: ["they", "them"],
    72: "the",
    73: "and",
    74: "to",
    75: "be",
    76: "of",
    77: ["we", "us"],
    78: "for",
    79: "think",
    80: "what",
    81: "with",
    82: "who",
    83: "why",
    84: "when",
    85: "how",
    86: "where",
    87: "lumi",
    88: "alex",
    89: "from",
    #   90 - 99 currently unused.
}
