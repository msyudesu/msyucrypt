#   Main PC
# main_folder = "C:\\Users\\msyud\\Desktop\\Code Projects\\MsyuCrypt\\Text Docs\\"
#   Work PC
main_folder = "C:\\Users\\alexclar\\OneDrive - Agilent Technologies\\Desktop\\Python Scraps\\Msyucrypt\\Text Docs\\"

plaintext_filename = "plaintext.txt"
ciphertext_filename = "vigenere_ciphertext.txt"
key_filename = "key.txt"
msyu_filename = "msyu_ciphertext.txt"
key_filepath = main_folder + key_filename
cipher_filepath = main_folder + ciphertext_filename
msyu_cipher_filepath = main_folder + msyu_filename
plain_filepath = main_folder + plaintext_filename

def read_plaintext_file():
    with open(plain_filepath, mode='r') as plaintext_file:
        import re

        plaintext = plaintext_file.read()
        plaintext = re.sub('[^A-z]', '', plaintext) #    Found Online: Removes special characters ".,-+=!@#$%^&*()\'\"?_|~\n\t\r"
    return plaintext

def read_key_from_file():
    key = []
    with open(key_filepath, mode='r') as key_file:
        import re
        key = key_file.read()
        key = re.sub('[^A-z]', '', key)
    return key

def generate_key_file(key):
    with open(key_filepath, mode='w') as key_file:
        key_file.write(key)

def generate_ciphertext_file(ciphertext):
    with open(cipher_filepath, mode='w') as ciphertext_file:
        column_id = 0
        for item in ciphertext:
            ciphertext_file.write(item + " ")
            column_id += 1
            if column_id == 15:
                column_id = 0
                ciphertext_file.write("\n")

def generate_msyu_ciphertext_file(msyu_ciphertext):
    with open(msyu_cipher_filepath, mode='w') as msyu_ciphertext_file:
        column_id = 0
        spacer = 0
        for i in range(len(msyu_ciphertext)):
            msyu_ciphertext_file.write(msyu_ciphertext[i])
            spacer += 1
            if spacer == 3:
                spacer = 0
                column_id += 1
                msyu_ciphertext_file.write(" ")
            if column_id == 15:
                column_id = 0
                msyu_ciphertext_file.write("\n")
                

def write_value_check(plaintext, key, ciphertext, original_text, msyu_ciphertext):
    #   Value Checks
    with open(main_folder + "review.txt", mode='w') as review_file:
        #   Full plaintext
        review_file.write("Plain text:\t\t")
        for i in range(len(plaintext)):
            review_file.write(plaintext[i] + "\t")
        review_file.write("\n")
        
        #   Full Cipher Text
        review_file.write("Cipher text:\t")
        for i in range(len(ciphertext)):
            review_file.write(ciphertext[i] + "\t")
        review_file.write("\n")
        
        #   Full Key
        review_file.write("Key text:\t\t")
        for i in range(len(key)):
            review_file.write(key[i] + "\t")
        review_file.write("\n")
        
        #   Full decipher text
        review_file.write("Decipher text:\t")
        for i in range(len(original_text)):
            review_file.write(original_text[i] + "\t")
        review_file.write("\n")
        ########################################
        review_file.write("#"*25 +  "UNICODE" + "#"*25 + "\n")
        #   Plaintext in Unciode value
        review_file.write("Plaintext:\t\t")
        for i in range(len(plaintext)):
            review_file.write(str(ord(plaintext[i])) + "\t")
        review_file.write("\n")
        
        #   Ciphertext in Unciode value
        review_file.write("Ciphertext:\t\t")
        for i in range(len(ciphertext)):
            review_file.write(str(ord(ciphertext[i])) + "\t")
        review_file.write("\n")
        
        #   Key in Unciode value
        review_file.write("Key:\t\t\t")
        for i in range(len(key)):
            review_file.write(str(ord(key[i])) + "\t")
        review_file.write("\n")
        
        #   Decipher text in Uncide value
        review_file.write("Deciphered:\t\t")
        for i in range(len(original_text)):
            review_file.write(str(ord(original_text[i])) + "\t")
        review_file.write("\n")

        #   Checking the difference between them.
        review_file.write("Deviation:\t\t")
        for i in range(len(plaintext)):
            review_file.write(str((ord(plaintext[i]) - ord(original_text[i]))) + "\t")

        #   Msyu Method Cipher
        review_file.write("\n")
        review_file.write("Msyu Crypto: \t")
        spacer = 0
        for i in range(len(msyu_ciphertext)):
            review_file.write(msyu_ciphertext[i])
            spacer += 1
            if spacer == 3:
                spacer = 0
                review_file.write("\t")