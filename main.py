from Crypto import msyucrypt, msyucrypt_file_writer, std_vigenere

plaintext = msyucrypt_file_writer.read_plaintext_file()
key = std_vigenere.generate_key(plaintext)
ciphertext = std_vigenere.encipher(plaintext, key)
msyu_ciphertext = msyucrypt.msyucrypt_transform(ciphertext, key)

msyucrypt_file_writer.generate_key_file(key)
msyucrypt_file_writer.generate_ciphertext_file(ciphertext)
msyucrypt_file_writer.generate_msyu_ciphertext_file(msyu_ciphertext)