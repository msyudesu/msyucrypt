def generate_key(plaintext):
    import random

    key = []
    for i in range(len(plaintext)):
        if (65 <= ord(plaintext[i]) <= 90):
            key.append(chr(random.randint(65, 90)))
        else:
            key.append(chr(random.randint(97, 122)))
    return ''.join(key)

def match_given_keyspace(key, plaintext):
    key = list(key)
    if len(key) == len(plaintext):
        return ''.join(key)
    else:
        for i in range(len(plaintext) - len(key)):
            key.append(key[i % len(key)])
        return ''.join(key)

def encipher(plaintext, key):
    ciphertext = []
    for i in range(len(plaintext)):
        if (65 <= ord(plaintext[i]) <= 90):
            ciphertext.append(chr(((ord(plaintext[i]) + ord(key[i])) % 26) + 65))
        else:
            ciphertext.append(chr(((ord(plaintext[i]) + ord(key[i])) % 26) + 97))
    return ''.join(ciphertext)

def decipher(ciphertext, key):
    deciphertext = []
    for i in range(len(ciphertext)):
        if (65 <= ord(ciphertext[i]) <= 90):
            deciphertext.append(chr(((ord(ciphertext[i]) - ord(key[i])) % 26) + 65))
        else:
            deciphertext.append(chr(((ord(ciphertext[i]) - ord(key[i]) + 92) % 26) + 97))
    return ''.join(deciphertext)